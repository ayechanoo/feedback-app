import { createContext, useState } from "react";
export const FeedbackContext = createContext(null);
export const FeedbackProvider = ({children}) => {
    const [feedback, setFeedBack] = useState([
        {
            id:1,
            text:'hello world,aco',
            rating:2
        },
        {
            id:1,
            text:'hello world,aco',
            rating:3
        }
    ])

    const [feedbackEdit,setEditFeedBack] = useState({
        item:{},
        edit:false
    })

    //adding feedback
    const createFeedback = (newItem) => {
        setFeedBack((feedbacks) => ([newItem, ...feedbacks]))
    }

    //editing feedback
    const updateFeedback = (id, data) => {
       setFeedBack(
        feedback.map((item) => (item.id === id ? {...item,...data}:item))
       );
    }

    const editFeedback = (item) => {
       
        setEditFeedBack({
        item:item,
        edit:true
        })
    }

    //deleting feedback
    const deleteFeedBack = (id) => {
        if(window.confirm('Are you sure to delete?')){
        setFeedBack((feedback) => feedback.filter((item) => item.id !== id));
        }
    }

    return <FeedbackContext.Provider value={{feedback, createFeedback, editFeedback,feedbackEdit, updateFeedback, deleteFeedBack}}>
        {children}
    </FeedbackContext.Provider>
}