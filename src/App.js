import './App.css';
import Header from './ components/Header';
import FeedBackList from './ components/FeedBackList';
import FeedBackStats from './ components/FeedBackStats';
import FeedbackForm from './ components/FeedbackForm';
import { BrowserRouter as Router, Route , Routes} from 'react-router-dom';
import AboutPage from './pages/AboutPage';
import AboutQuestionIcon from './ components/AboutQuestionIcon';
import { FeedbackProvider } from './context/FeedbackContext';

function App() {

  
  return (
    <FeedbackProvider>
      <Router>
        
        <Header />
        
          <div className="container">
            <Routes>
              <Route exact path="/" element={
                <>
                <FeedbackForm />
                <FeedBackStats />
                <FeedBackList/>
                </>
              }/>
              <Route path="/about" element={<AboutPage/>}/>
            </Routes>
            
          </div>
          <AboutQuestionIcon/>
      </Router>
      </FeedbackProvider>
  );
}

export default App;
