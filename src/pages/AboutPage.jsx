import React from 'react'
import Card from '../ components/shared/Card'
import { Link } from 'react-router-dom';

function AboutPage() {
    return (
        <Card>
            <div className='about'>
                <h2>About this project</h2>
                <p>This is a react app to leave feedback for a product or service</p>
                <p>Version: 1.0.0.</p>
            </div>
            <Link to="/">Back to home page</Link>
        </Card>
    )
}

export default AboutPage
