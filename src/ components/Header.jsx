function Header(props) {
    const styles = {
        backgroundColor:props?.bgColor,
        color:props?.textColor
    }
    return (
        <header style={styles}>
            <div className='container'>
                <h2>{props?.text}</h2>
            </div>
        </header>
    )
}

Header.defaultProps = {
    text: 'FeedBack UI',
    bgColor:'#303030',
    textColor:'#fff'
    
}

export default Header
