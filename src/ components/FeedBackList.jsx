import FeedBackItem from './FeedBackItem'
import { motion, AnimatePresence } from "framer-motion"
import { FeedbackContext } from '../context/FeedbackContext'
import { useContext } from 'react'

export default function FeedBackList() {
	const {feedback} = useContext(FeedbackContext);
	if(feedback.length === 0) {
		return <p>There is no feedback</p>
	}
	return (
		<div className="feedback-list">
			<AnimatePresence>
			{feedback.map((item, key) => (
				<motion.div 
				key={key}
				initial={{opacity:0}}
				animate={{opacity:1}}
				exit={{opacity:0}}
				layout
				>
				<FeedBackItem key={item.id} item={item} />
				</motion.div>) )}
			</AnimatePresence>
		</div>
	)
}