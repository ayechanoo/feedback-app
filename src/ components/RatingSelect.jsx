import {useState, useContext, useEffect} from 'react';
import {FeedbackContext} from './../context/FeedbackContext';

function RatingSelect({select}) {
    const [selected, setSelected] = useState(1);
    const {feedbackEdit} = useContext(FeedbackContext);

    useEffect(() => {
      
      setSelected(feedbackEdit.item.rating);
     
  }, [feedbackEdit])


    const handleChange = (e) => {
       setSelected(+e.currentTarget.value);
       select(+e.currentTarget.value);
    }

  
    

    return (
        <ul className='rating'>
            <li >
                <input
                    type='radio'
                    id="1"
                    name='rating'
                    value={1}
                    checked={selected === 1}
                    onChange={handleChange}
                />
                <label htmlFor="1">1</label>
            </li>
            <li >
                <input
                    type='radio'
                    id="2"
                    name='rating'
                    value={2}
                    checked={selected === 2}
                    onChange={handleChange}
                />
                <label htmlFor="2">2</label>
            </li>

            <li >
                <input
                    type='radio'
                    id="3"
                    name='rating'
                    value={3}
                    checked={selected === 3}
                    onChange={handleChange}
                />
                <label htmlFor="3">3</label>
            </li>

            <li >
                <input
                    type='radio'
                    id="4"
                    name='rating'
                    value={4}
                    checked={selected === 4}
                    onChange={handleChange}
                />
                <label htmlFor="4">4</label>
            </li>
            <li >
                <input
                    type='radio'
                    id="5"
                    name='rating'
                    value={5}
                    checked={selected === 5}
                    onChange={handleChange}
                />
                <label htmlFor="5">5</label>
            </li>
            
        </ul>
    )
}

export default RatingSelect
