import { useState, useEffect, useContext } from "react"
import { v4 as uuidv4 } from 'uuid';
import Card from "./shared/Card"
import Button from "./shared/Button";
import RatingSelect from "./RatingSelect";
import { FeedbackContext } from "../context/FeedbackContext";
function FeedbackForm() {
    const [text, setText]= useState('');
    const [rating, setRating] = useState(1);
    const [msg, setMsg]= useState(null);
    const [btnDisabled, setBtnDisabled]= useState(true);
    const {createFeedback, updateFeedback, feedbackEdit} = useContext(FeedbackContext);

  

    const validation =(text) => {
        if(text === '') {
        setMsg(null);
        setBtnDisabled(true);
        }else if(text !== '' && text.trim().length <=10) {
            setMsg('at least 10 characters required!');
            setBtnDisabled(true);
        }else{
            setMsg(null);
            setBtnDisabled(false);
        }
    }

    const handleTextChange = (e) => {
        const {value} = e.target;
        
        validation(value);
        setText(value);
        
    }

    const handleSubmit = (e) => {
        e.preventDefault();
       const newFeedBack = {
        id:uuidv4(),
        text:text,
        rating:rating
       }
       if(feedbackEdit.edit === true) {
        updateFeedback(feedbackEdit.item.id, newFeedBack)
       }else{
        createFeedback(newFeedBack);
       }

       setText('');
       

    }

    
    useEffect(()=> {
        if(feedbackEdit.edit === true) {
            setBtnDisabled(false);
            setText(feedbackEdit.item.text);
            setRating(feedbackEdit.item.rating);
        }
    },[feedbackEdit])
    
    return (
        <Card>
            <form onSubmit={handleSubmit}>
                <h2>How would you rate your services with people?</h2>
                <RatingSelect select={(rating) => setRating(rating)} />
                <div className="input-group">
                    <input type="text" placeholder="Write a review" value={text} onChange={handleTextChange}/>
                    <Button type="submit" isDisabled={btnDisabled}> Send it! </Button>
                </div>
                {msg && <div className="message">{msg}</div>}
            </form>
        </Card>
    )
}

export default FeedbackForm
