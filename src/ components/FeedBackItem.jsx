import { useContext } from 'react';
import Card from './shared/Card';
import {FaTimes, FaEdit} from 'react-icons/fa';
import {FeedbackContext} from './../context/FeedbackContext';
function FeedBackItem({ item, handleDelete }) {
  const {deleteFeedBack, editFeedback} = useContext(FeedbackContext);
    return (
        <Card>
            <div className="num-display">{item?.rating}</div>
            <button className="close" onClick={()=>deleteFeedBack(item.id)}><FaTimes/></button>
            <button className="edit" onClick={()=>editFeedback(item)}><FaEdit/></button>
            <div className="text-display">{item?.text}</div>
        </Card>
    )
}


export default FeedBackItem
